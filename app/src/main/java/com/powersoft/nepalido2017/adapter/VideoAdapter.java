package com.powersoft.nepalido2017.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.powersoft.nepalido2017.R;
import com.powersoft.nepalido2017.VideoEntity;

import java.util.ArrayList;

/**
 * Created by sailesh on 6/16/17.
 */

public class VideoAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<VideoEntity> listVideo;

    public VideoAdapter(Context context, ArrayList<VideoEntity> listVideo) {
        this.context = context;
        this.listVideo = listVideo;
    }

    @Override
    public int getCount() {
        return listVideo.size();
    }

    @Override
    public Object getItem(int position) {
        return listVideo.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View v, ViewGroup parent) {
        v = null;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        v = inflater.inflate(R.layout.item_video, null);

        LinearLayout ll = (LinearLayout) v.findViewById(R.id.ll);
        SimpleDraweeView image = (SimpleDraweeView) v.findViewById(R.id.imgVideo);
        TextView txtTitle = (TextView) v.findViewById(R.id.txtTitle);

        image.setImageURI(Uri.parse("http://img.youtube.com/vi/" + listVideo.get(position).videoCode + "/0.jpg"));
        txtTitle.setText(listVideo.get(position).title);

        ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String videoId = listVideo.get(position).videoCode;
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:" + videoId));
                intent.putExtra("VIDEO_ID", videoId);
                context.startActivity(intent);
            }
        });

        return v;
    }
}
