package com.powersoft.nepalido2017.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.powersoft.nepalido2017.R;
import com.powersoft.nepalido2017.VideoEntity;
import com.powersoft.nepalido2017.adapter.VideoAdapter;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;

public class FragmentVideos extends Fragment {

    private ListView listView;
    private ArrayList<VideoEntity> listVideo;
    private VideoAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_videos, container, false);

        listView = (ListView) v.findViewById(R.id.listView);

        FirebaseDatabase.getInstance().setPersistenceEnabled(true);

        listVideo = new ArrayList<>();
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
        databaseReference.keepSynced(true);
        databaseReference.child("Videos").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                listVideo.clear();
                listVideo = getVideoList(dataSnapshot.getValue().toString());
                adapter = new VideoAdapter(getContext(), listVideo);
                listView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        return v;
    }

    private ArrayList<VideoEntity> getVideoList(String str) {
        ArrayList<VideoEntity> listVideo = new ArrayList<>();
        String string = str.replace("{", "");
        string = string.replace("}", "");
        String[] key = StringUtils.split(string, ",");
        for (String k : key) {
            String[] data = StringUtils.split(k, "=");
            listVideo.add(new VideoEntity(data[1], data[0]));
        }
        return listVideo;
    }
}
