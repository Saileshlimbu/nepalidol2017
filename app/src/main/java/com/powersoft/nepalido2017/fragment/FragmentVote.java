package com.powersoft.nepalido2017.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.telephony.SmsManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.powersoft.nepalido2017.R;

public class FragmentVote extends Fragment {


    private Button btnVote;
    private EditText txtVoteAmount;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_vote, container, false);

        btnVote = (Button) v.findViewById(R.id.btnVote);
        txtVoteAmount = (EditText) v.findViewById(R.id.txtVoteAmount);

        btnVote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int amount = Integer.parseInt(txtVoteAmount.getText().toString());
                if (TextUtils.isEmpty(txtVoteAmount.getText().toString())) {
                    txtVoteAmount.setError("Please enter number of vote.");
                    return;
                }
                sendSMS(amount, "35225", "07");
            }
        });

        return v;
    }

    public void sendSMS(int amount, String number, String message) {
        SmsManager smsManager = SmsManager.getDefault();
        for (int i = 0; i < amount; i++) {
            smsManager.sendTextMessage(message, null, message, null, null);
        }
    }
}
