package com.powersoft.nepalido2017.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.powersoft.nepalido2017.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import static android.app.Activity.RESULT_OK;

public class FragmentCreateImage extends Fragment {

    private static Context context;
    private Button loadImage;
    private Button saveImage;
    private ImageView image;

    Uri source1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_create_image, container, false);

        loadImage = (Button) v.findViewById(R.id.loadImage);
        saveImage = (Button) v.findViewById(R.id.saveImage);
        image = (ImageView) v.findViewById(R.id.imageView);
        context = getContext();


        loadImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, 1);
            }
        });

        saveImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                image.setDrawingCacheEnabled(true);
                savebitmap(image.getDrawingCache());
                Toast.makeText(getContext(), "Image Saved!!!", Toast.LENGTH_SHORT).show();
            }
        });

        return v;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case 1:
                    source1 = data.getData();
                    Bitmap processedBitmap = ProcessingBitmap();
                    if (processedBitmap != null) {
                        image.setImageBitmap(processedBitmap);
                        saveImage.setVisibility(View.VISIBLE);
                    } else {
                        Toast.makeText(getContext(), "Something wrong in processing!", Toast.LENGTH_LONG).show();
                    }
                    break;
            }
        }

    }

    private Bitmap ProcessingBitmap() {
        Bitmap bm1 = null;
        Bitmap bm2 = null;
        Bitmap newBitmap = null;
        try {
            InputStream istr = getContext().getAssets().open("image.png");

            bm1 = BitmapFactory.decodeStream(getContext().getContentResolver().openInputStream(source1));
            bm2 = BitmapFactory.decodeStream(istr);

            int w = bm1.getWidth();
            int h = bm1.getHeight();

            int height = w / (int) 5.26f;

            bm2 = Bitmap.createScaledBitmap(bm2, w, height, false);

            Bitmap.Config config = bm1.getConfig();
            if (config == null) {
                config = Bitmap.Config.ARGB_8888;
            }

            newBitmap = Bitmap.createBitmap(w, h, config);
            Canvas newCanvas = new Canvas(newBitmap);

            newCanvas.drawBitmap(bm1, 0, 0, null);

            Paint paint = new Paint();
            paint.setAlpha(230);
            newCanvas.drawBitmap(bm2, 0, h - height, paint);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return newBitmap;
    }

    public static File savebitmap(Bitmap bmp) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 60, bytes);
        File f = new File(Environment.getExternalStorageDirectory()
                + File.separator + "supportNishan.jpg");
        try {
            if (f.exists()) {
                f.delete();
            }
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return f;
    }
}
