package com.powersoft.nepalido2017.service;

import android.app.Notification;
import android.os.Bundle;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.powersoft.nepalido2017.MainActivity;
import com.powersoft.nepalido2017.R;

import java.util.Map;

import br.com.goncalves.pugnotification.notification.PugNotification;

/**
 * Created by sailesh on 5/14/17.
 */


public class PushNotification extends FirebaseMessagingService {
    private static final String TAG = "FCM Service";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        String body = remoteMessage.getNotification().getBody();

        Map<String, String> data = remoteMessage.getData();
        Bundle bundle = new Bundle();
        PugNotification.with(getApplicationContext())
                .load()
                .title("New Video")
                .message(body)
                .smallIcon(R.mipmap.ic_launcher)
                .largeIcon(R.mipmap.ic_launcher)
                .flags(Notification.DEFAULT_ALL)
                .flags(Notification.FLAG_AUTO_CANCEL)
                .click(MainActivity.class, bundle)
                .simple()
                .build();
    }
}
