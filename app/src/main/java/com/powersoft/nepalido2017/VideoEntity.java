package com.powersoft.nepalido2017;

/**
 * Created by sailesh on 6/16/17.
 */

public class VideoEntity {
    public String videoCode;
    public String title;

    public VideoEntity(String videoCode, String title) {
        this.videoCode = videoCode;
        this.title = title;
    }
}
